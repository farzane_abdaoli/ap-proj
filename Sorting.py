import container as vec
import random as rand
from time import process_time
############################BUBBLE SORT FUNC##############################################
def bubble_sort(v):
     # Traverse through all array elements
    for i in range(v.len()):
        
         # Last i elements are already in place
         for j in range(0, v.len()-i-1):
             
                 # traverse the array from 0 to n-i-1
                 # Swap if the element found is greater
                 # than the next element
                 if v[j] > v[j+1] :
                     v[j], v[j+1] = v[j+1], v[j]
                     #my_vec.show()                          
    return v

###########################SELECTION SORT FUNC#############################################
def selection_sort(v):
    # Traverse through all array elements
    for i in range(v.len()):
    
        # Find the minimum element in remaining 
        # unsorted array
        min_idx = i
        for j in range(i+1, v.len()):
            if v[min_idx] >v[j]:
                min_idx = j
            
    # Swap the found minimum element with 
    # the first element        
        v[i], v[min_idx] = v[min_idx], v[i]
    
    return v
################################QUICK SORT FUNC##########################################  
def quick_sort(arr,low,high):
    if low < high:  
        i = ( low-1 )         # index of smaller element
        pivot = arr[high]     # pivot
    
        for j in range(low , high):
            if   arr[j] <= pivot:
                i = i+1 
                arr[i],arr[j] = arr[j],arr[i]
    
        arr[i+1],arr[high] = arr[high],arr[i+1]
        pi=i+1
        quick_sort(arr, low, pi-1)
        quick_sort(arr, pi+1, high)
    return arr

def partition(arr,low,high):
    i = ( low-1 )         # index of smaller element
    pivot = arr[high]     # pivot

    for j in range(low , high):
        # If current element is smaller than or
        # equal to pivot
        if   arr[j] <= pivot:
            # increment index of smaller element
            i = i+1 
            arr[i],arr[j] = arr[j],arr[i]

    arr[i+1],arr[high] = arr[high],arr[i+1]
    return ( i+1 )
##########################################MERGE SORT####################33
def mergeSort(alist):
    if alist.len()>1:
        mid = alist.len()/2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i < lefthalf.len() and j < righthalf.len():
            if lefthalf[i] < righthalf[j]:
                alist[k]=lefthalf[i]
                i=i+1
            else:
                alist[k]=righthalf[j]
                j=j+1
            k=k+1

        while i < lefthalf.lan():
            alist[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j < righthalf.lan():
            alist[k]=righthalf[j]
            j=j+1
            k=k+1
    return(alist)
#    




 ##################################TEST PART#########################################   
#N=int(input("please enter size of container:"))
#my_vec=vec.container(N)
#for i in range(N):
#    my_vec.push_back(rand.randint(0,101))
##my_vec.show()
#
##t0=process_time()
##sorted_vec1=mergeSort(my_vec)
##t1=process_time()
##sorted_vec1.show()
##print(t1-t0)
#
##t2=process_time()
##sorted_vec2=selection_sort(my_vec)
##t3=process_time()
###sorted_vec2.show()
##print(t3-t2)
##
#t4=process_time()
#sorted_vec3=quick_sort(my_vec,0,N-1)
#t5=process_time()
#sorted_vec3.show()
#print(t5-t4)
#
#
##sorted_vec4 =mergeSort(my_vec)
##sorted_vec4.show()