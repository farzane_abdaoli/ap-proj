import container as vec
import Sorting
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication,QMainWindow,QPushButton,QLineEdit,QLabel
from PyQt5.QtGui import QPainter,QBrush,QPen
from PyQt5.QtCore import Qt
import sys
import numpy as np
from time import sleep
from PyQt5.QtCore import QThread 
from PyQt5 import QtCore
from time import process_time
import time

class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        QMainWindow.__init__(self)
        self.n=0 #number of rectangles
        self.painter=[]#list of rectangles
        self.flag=False#flags for painting
        self.flag1=False
        self.initwindow()
        self.l=[]
        
    def initwindow(self):
        #creating main window
        self.setWindowTitle("Graphic Sorting")
        self.setGeometry(100,100,self.n*20+40,500)
        #creating buttons
        self.b=QPushButton("generate random data",self)
        self.b.setStyleSheet("background-color: rgb(102, 255, 153);")
        self.b.resize(180,30)
        self.b.move(300,10)
        
        #creating sort button
        self.s1=QPushButton("Bubble Sort",self)
        self.s1.setStyleSheet("background-color: rgb(153, 255, 204);")
        self.s1.resize(100,30)
        self.s1.move(500,10)
        
        self.s2=QPushButton("Selection Sort",self)
        self.s2.setStyleSheet("background-color: rgb(204, 255, 255);")
        self.s2.resize(100,30)
        self.s2.move(620,10)
        
 
        self.s5=QPushButton("Insert Sort",self)
        self.s5.setStyleSheet("background-color: rgb(204, 204, 255);")
        self.s5.resize(100,30)
        self.s5.move(740,10)
        
        self.s6=QPushButton("Compare",self)
        self.s6.setStyleSheet("background-color: rgb(204, 153, 255);")
        self.s6.resize(100,30)
        self.s6.move(1230,10)
    
        
        #creating lable
        self.inputLabel = QLabel(self)
        self.inputLabel.setText('Enter a number: ')
        self.inputLabel.move(10, 10)
        
        self.inputLabel1 = QLabel(self)
        self.inputLabel1.setText('Took: ')
        self.inputLabel1.move(10, 80)
        
        self.inputLabel2 = QLabel(self)
        self.inputLabel2.setText('Took for bubble_sort: ')
        self.inputLabel2.resize(140,30)
        self.inputLabel2.move(1130, 80)
        
        self.inputLabel3 = QLabel(self)
        self.inputLabel3.setText('Took for selection_sort: ')
        self.inputLabel3.resize(140,30)
        self.inputLabel3.move(1130, 110)
        
        self.inputLabel4 = QLabel(self)
        self.inputLabel4.setText('Took for insertion_sort: ')
        self.inputLabel4.resize(140,30)
        self.inputLabel4.move(1130, 140)
        
        
        
        #creating text edit
        self.textbox = QLineEdit(self)
        self.textbox.resize(80,30)
        self.textbox.move(105, 10)
        
        self.textbox1 = QLineEdit(self)
        self.textbox1.resize(80,30)
        self.textbox1.move(105, 80)
        
        self.textbox2 = QLineEdit(self)
        self.textbox2.resize(200,20)
        self.textbox2.move(1300, 85)
        
        self.textbox3 = QLineEdit(self)
        self.textbox3.resize(200,20)
        self.textbox3.move(1300, 115)
        
        self.textbox4 = QLineEdit(self)
        self.textbox4.resize(200,20)
        self.textbox4.move(1300, 145)
        
        self.textbox5 = QLineEdit(self)
        self.textbox5.resize(550,40)
        self.textbox5.move(300, 80)
        f = self.textbox5.font()
        f.setPointSize(16) # sets the size to 27
        self.textbox5.setFont(f)
        self.showMaximized()#to have the window in maximum size
        
        #clicked actions
        self.b.clicked.connect(self.random_clicked)
        self.s1.clicked.connect(self.sortbutton1)
        self.s2.clicked.connect(self.sortbutton2)
        self.s5.clicked.connect(self.sortbutton5)
        self.s6.clicked.connect(self.compare)
        
        
        

        

    def random_clicked(self):
        self.n=int(self.textbox.text())#read number from the text box
        self.len=vec.container(self.n)#my defined list
        for i in range(self.n):
            self.len.push_back(np.random.randint(1,101))#pushing random numbers in the list
        print('random numbers:')
        self.len.show()
        self.paint()

        
    def paint(self):  #start the paint event
        self.flag=True
        self.repaint()
        
    def paint1(self):  
        self.flag1=True
        self.repaint()

        
    def paintEvent(self,e):
        if(self.flag):
            for i in range(self.n):#painting rects according to lengthes
                self.painter.append(QPainter(self))
                self.painter[i].setPen(QPen(Qt.NoPen))
                self.painter[i].setBrush(QBrush(Qt.blue,Qt.SolidPattern))
                self.painter[i].drawRect(20+(20*i),430-2*self.len[i],15,2*self.len[i])
            self.flag=False
        if(self.flag1):
            for i in range(self.n,2*self.n):#painting rects according to lengthes
                self.painter.append(QPainter(self))
                self.painter[i].setPen(QPen(Qt.NoPen))
                self.painter[i].setBrush(QBrush(Qt.blue,Qt.SolidPattern))
                self.painter[i].drawRect(20+(20*(i-self.n)),900-2*self.len[i-self.n],15,2*self.len[i-self.n])
            self.flag1=False
            
    #bubble sort 
    def sortbutton1(self):
        self.textbox5.setText("Oach,your choice is the most stupid one.")
        name=self.s1.text()
        t1=process_time()
        self.sort(name)
        self.paint() 
        while(self.plot_thread.tflag):
            self.sort(name)
            self.paint() 
            sleep(0.3)
        t2=process_time()
        print("Took:{:f}".format(t2-t1))
        self.textbox1.setText(str(t2-t1))
        QApplication.processEvents()
    #selection sort 
    def sortbutton2(self):
        self.textbox5.setText("Somehow better than previous one.")
        name=self.s2.text()
        t1=process_time()
        n=0
        self.sort(name,n)
        self.paint() 
        while(self.plot_thread.tflag):
            n+=1
            self.sort(name,n)
            self.paint() 
            sleep(0.3)
        t2=process_time()
        print("Took:{:f}".format(t2-t1))
        self.textbox1.setText(str(t2-t1))
        QApplication.processEvents()
        
    def sortbutton5(self):
        self.textbox5.setText("Nice choice.")
        n=1
        name=self.s5.text()
        t1=process_time()
        print("s")
        self.sort(name,n)
        self.paint()
        
        
        while(self.plot_thread.tflag):
            n+=1
            print("while")
            self.sort(name,n)
            self.paint() 
            sleep(0.3)
        t2=process_time()
        print("Took:{:f}".format(t2-t1))
        self.textbox1.setText(str(t2-t1))
        QApplication.processEvents()
        
    
        
    
    def sort(self,name,n=0,n1=0):
        self.flag=True
        self.repaint()
        self.plot_thread=Mythread(self.len,name,n,n1)
        self.plot_thread.start()
        QApplication.processEvents()
       
    def compare(self):
        
        self.n=int(self.textbox.text())#read number from the text box
        for i in range(self.n):
            self.l.append(np.random.randint(1,101))
        #bubble sort:
        t1=time.clock()
        self.v=self.l.copy()
        for i in range(len(self.v)):
            for j in range(0, len(self.v)-i-1):
                 if self.v[j] > self.v[j+1] :
                     self.v[j],self. v[j+1] = self.v[j+1], self.v[j]
        t2=time.clock()
        self.textbox2.setText(str(t2-t1))
        
        #selection sort:
        self.v1=self.l.copy()
        t3=time.clock()
        for i in range(len(self.v1)):
            min_idx = i
            for j in range(i+1, len(self.v1)):
                if self.v1[min_idx] >self.v1[j]:
                    min_idx = j        
            self.v1[i], self.v1[min_idx] = self.v1[min_idx], self.v1[i]
        t4=time.clock()
        self.textbox3.setText(str(t4-t3))
        
        #insertion_sort:
        self.v2=self.l.copy()
        t5=time.clock()
        for index in range(1,len(self.v2)):
            currentvalue = self.v2[index]
            position = index

            while position>0 and self.v2[position-1]>currentvalue:
                self.v2[position]=self.v2[position-1]
                position = position-1

            self.v2[position]=currentvalue
        t6=time.clock()
        self.textbox4.setText(str(t6-t5))
#updating thread
class Mythread(QThread): 
    def __init__(self,my_list,name,n=0,n1=0):
        QThread.__init__(self)
        self.list=my_list
        self.tflag=True
        self.func=name
        self.n=n
        self.n1=n1
        self.pi=0
        self.i=0
    def run(self):
        if self.func=="Bubble Sort":
            self.list=self.bubble_sort(self.list)
        if self.func=="Selection Sort":
            self.list=self.selection_sort(self.list)
        if self.func=="Insert Sort":
            self.list=self.insertion_sort(self.list)
        

        
    def bubble_sort(self,v):
        for i in range(v.len()):
            if i==v.len()-1:
                self.tflag=False
            for j in range(0, v.len()-i-1):
                     if v[j] > v[j+1] :
                         v[j], v[j+1] = v[j+1], v[j]
                         QApplication.processEvents()
                         return v

    def selection_sort(self,v):
        for i in range(self.n,v.len()+1):
            if i==v.len():
                self.tflag=False
            min_idx = i
            for j in range(i+1, v.len()):
                if v[min_idx] >v[j]:
                    min_idx = j   
            v[i], v[min_idx] = v[min_idx], v[i]
            QApplication.processEvents()
            return v
        
    def insertion_sort(self,arr):
        for i in range(self.n, arr.len()+1):
            if i==arr.len():
                self.tflag=False
                arr.show()
            key = arr[i]
            j = i-1
            while j >=0 and key < arr[j] :
                arr[j+1] = arr[j]
                j -= 1
            arr[j+1] = key
            QApplication.processEvents()
            return arr
        
    
        
        
            
App=QApplication(sys.argv)
window=Window()
sys.exit(App.exec())